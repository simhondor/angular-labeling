export interface Coordinates {
  endX: number;
  endY: number;
  startY: number;
  startX: number;
}

export interface TestSubject {
  number: number;
  Subject: string;
}

export interface ImageStyle {
  'height': string;
  'width': string;
  scale: number;
  'top': string;
  'left': string;
  'background-image': string;
  'background-repeat': string;
  'background-size': string;

}

export interface Selection {
  number: number;
  class: string;
  startX: number;
  endX: number;
  startY: number;
  endY: number;
}

export interface TagNumber {
  class: string;
  count: number;
}

export interface Image {
  src: string;
  width: number;
  height: number;
  scale: number;
  top: number;
  left: number;
}
