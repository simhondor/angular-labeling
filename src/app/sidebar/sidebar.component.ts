import {Component, OnInit} from '@angular/core';
import {Selection, TagNumber, TestSubject} from '../interfaces';
import {SubjectService} from '../services/subject.service';
import {SelectionService} from '../services/selection.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  TestSubjects: TestSubject[];
  activeSubjectTable = 0;
  activeSubject: TestSubject;
  selectionArray: Selection[];
  activeSelection: number;

  constructor(private subjectService: SubjectService, private selectionService: SelectionService) {
  }

  ngOnInit() {
    this.TestSubjects = this.subjectService.getTestSubjects();
    this.subjectService.subject.subscribe((data: TestSubject) => {
      this.activeSubject = data;
    });
    this.selectionArray = this.selectionService.getSelectionArray();
    this.setCurrentSelectionSubject(this.subjectService.testSubjects[0]);
  }

  setActiveSubject(i: number, subject: TestSubject): void {
    this.activeSubjectTable = i;
    this.setCurrentSelectionSubject(subject);
  }

  setActiveSelection(i: number): void {
    this.activeSelection = i;
  }

  setCurrentSelectionSubject(subject: TestSubject): void {
    this.subjectService.setCurrentSubject(subject.Subject);
  }

  clearAllSelections(): void {
    this.selectionService.clearAllSelections();
  }

  hideSelection(i: number): void {
    this.selectionService.toggleSelectionDisplay(i);
  }

  deleteSelection(i: number): void {
    this.selectionService.clearSelection(i);
  }

  countNumberOfTags(subjectName: string) {
    return this.selectionService.countTags(subjectName);
  }

  getAreaSize(i: Selection) {
    return this.selectionService.calculateSelectionArea(i);
  }

  exportSelectionsToJSON(valuesToExport: Selection[]) {
    console.log(JSON.stringify(valuesToExport));
  }
}

