import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {SidebarComponent} from './sidebar/sidebar.component';
import {PlaygroundComponent} from './playground/playground.component';
import {ImageService} from './services/image.service';
import {SelectionBoxComponent} from './selection-box/selection-box.component';
import {SubjectService} from './services/subject.service';
import {SelectionService} from './services/selection.service';
import {ModalComponent} from './modal/modal.component';

@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    PlaygroundComponent,
    SelectionBoxComponent,
    ModalComponent,
  ],
  imports: [
    BrowserModule,
  ],
  providers: [ImageService, SubjectService, SelectionService],
  bootstrap: [AppComponent],
  entryComponents: [
    SelectionBoxComponent
  ]
})
export class AppModule {
}
