import {EventEmitter, Injectable} from '@angular/core';
import {Coordinates, Image} from '../interfaces';

@Injectable({
  providedIn: 'root'
})
export class ImageService {
  currentCoordinates: EventEmitter<Coordinates> = new EventEmitter();
  image: Image = {
    src: 'https://i.imgur.com/9RVulHk.jpg',
    width: 1024,
    height: 694,
    scale: 1,
    top: 0,
    left: 0
  };


  constructor() {
  }

  public getImage(): Image {
    return this.image;
  }

  public getImageBoundries() {
    // return document.getElementById('image').getBoundingClientRect();
  }
}
