import {Injectable} from '@angular/core';
import {TestSubject} from '../interfaces';
import {BehaviorSubject, Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SubjectService {
  currentSubject: BehaviorSubject<TestSubject> = new BehaviorSubject<TestSubject>({number: 0, Subject: 'Person'});
  subject: Observable<TestSubject> = this.currentSubject.asObservable();
  testSubjects: TestSubject[] = [{number: 1, Subject: 'Person'}, {number: 2, Subject: 'Dog'}, {number: 3, Subject: 'Balloon'}];

  constructor() {
  }

  setCurrentSubject(subject): void {
    this.currentSubject.next(subject);
  }

  getTestSubjects(): TestSubject[] {
    return this.testSubjects;
  }
}
