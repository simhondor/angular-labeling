import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';
import {Selection, TagNumber} from '../interfaces';
import {ImageService} from './image.service';

@Injectable({
  providedIn: 'root'
})
export class SelectionService {
  selectionSubject: Subject<Selection> = new Subject<Selection>();
  tagNumberSubject: Subject<TagNumber[]> = new Subject<TagNumber[]>();
  selectionArray: Selection[] = [];
  numberOfTagsArray: TagNumber[];

  constructor(private imageService: ImageService) {

    this.selectionSubject.subscribe({
      next: (selection: Selection) => this.selectionArray.push(selection)
    });
  }

  getSelectionArray(): Selection[] {
    return this.selectionArray;
  }

  clearAllSelections(): void {
    this.selectionArray.length = 0;
    const image = document.getElementById('image');
    while (image.firstChild) {
      image.removeChild(image.firstChild);
    }
  }

  toggleSelectionDisplay(i: number): void {
    const selection = document.getElementsByClassName('box')[i] as HTMLElement;
    if (selection.style.display === 'none') {
      selection.style.display = 'block';
    } else {
      selection.style.display = 'none';
    }
  }

  clearSelection(i: number): void {
    this.selectionArray.splice(i, 1);
    document.getElementsByClassName('box')[i].remove();
  }

  countTags(subjectClass: string) {
    const tempArray = this.selectionArray.map((selection) => {
      return selection.class;
    });
    this.numberOfTagsArray = tempArray.reduce((b, c) => ((b[b.findIndex(d => d.class === c)] || b[b.push({
      class: c,
      count: 0
    }) - 1]).count++, b), []);
    for (let i of this.numberOfTagsArray) {
      if (i.class === subjectClass) {
        return i.count;
      }
    }
  }

  calculateSelectionArea(i: Selection): string {
    const selection = this.getSelectionDimensions(i);
    const selectionArea = selection.width * selection.height;
    const image = this.imageService.getImage();
    const imageArea = image.height * image.width;
    return `${(selectionArea / imageArea * 100).toFixed(1)}%`;

  }

  getSelectionDimensions(selection: Selection) {
    const width = Math.abs(selection.startX - selection.endX);
    const height = Math.abs(selection.startY - selection.endY);
    return {
      height,
      width
    };
  }
}

