import {Component, Input, OnInit} from '@angular/core';
import {ImageService} from '../services/image.service';
import {Coordinates} from '../interfaces';

@Component({
  selector: 'app-selection-box',
  templateUrl: './selection-box.component.html',
  styleUrls: ['./selection-box.component.scss']
})
export class SelectionBoxComponent implements OnInit {
  @Input() boxStyle;
  private width: number;
  private height: number;
  private bounds:any;

  constructor(private imageService: ImageService) {
  }

  ngOnInit() {
    this.imageService.currentCoordinates.subscribe(data => {
        this.calculatePosition(data);
      },
    );
    console.log(this.boxStyle);
    this.bounds = this.imageService.getImageBoundries();
  }

  setBoxDimensions(x, y) {
    // this.boxDimensions = {
    //   position: this.boxStyle.position,
    //   top: this.boxStyle.top,
    //   left: this.boxStyle.left,
    //   width: this.boxStyle.width,
    //   height: this.boxStyle.height
    // };
  }

  calculatePosition(coordinates: Coordinates) {
    const x = coordinates.startX - this.bounds.left;
    const y = coordinates.startY - this.bounds.top;
    this.width = Math.abs(coordinates.startX - coordinates.endX);
    this.height = Math.abs(coordinates.startY - coordinates.endY);
    // this.setBoxDimensions(x, y);
  }
}
