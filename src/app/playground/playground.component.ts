import {Component, OnInit} from '@angular/core';
import {ImageService} from '../services/image.service';
import {Image, ImageStyle, Selection, TagNumber, TestSubject} from '../interfaces';
import {SelectionService} from '../services/selection.service';
import {SubjectService} from '../services/subject.service';

@Component({
  selector: 'app-playground',
  templateUrl: './playground.component.html',
  styleUrls: ['./playground.component.scss']
})
export class PlaygroundComponent implements OnInit {
  mousePressed: boolean;
  startingMouseX: number;
  startingMouseY: number;
  endMouseX: number;
  endMouseY: number;
  selectionArray: Selection[];
  CurrentX: number;
  CurrentY: number;
  private workingImage: Image;
  imageStyle: ImageStyle;
  bounds: ClientRect | DOMRect;
  private box: HTMLDivElement = null;
  private currentSubject: any;

  constructor(private imageService: ImageService, private selectionService: SelectionService, private subjectService: SubjectService) {
  }

  ngOnInit() {
    this.setInitialImageProperties();
    this.subjectService.subject.subscribe((data: TestSubject) => {
      this.currentSubject = data;
    });
    this.selectionArray = this.selectionService.getSelectionArray();
  }

  setInitialImageProperties(): void {
    this.workingImage = this.imageService.getImage();
    this.imageStyle = {
      'height': `${this.workingImage.height}px`,
      'width': `${this.workingImage.width}px`,
      scale: this.workingImage.scale,
      'top': `${this.workingImage.top}px`,
      'left': `${this.workingImage.left}px`,
      'background-image': `url(${this.workingImage.src})`,
      'background-repeat': 'no-repeat',
      'background-size': '100% auto'
    };
    const image = document.getElementById('image');
  }

  trackMouseMove(event: MouseEvent): void {
    if (this.mousePressed) {
      if (this.box !== null) {
        this.CurrentX = event.clientX;
        this.CurrentY = event.clientY;

        this.box.className = 'box';
        this.box.style.height = `${Math.abs(this.CurrentY - this.startingMouseY)}px`;
        this.box.style.width = `${Math.abs(this.CurrentX - this.startingMouseX)}px`;
        this.box.style.left = (this.CurrentX - this.startingMouseX < 0) ?
          `${this.CurrentX - this.bounds.left}px` :
          `${this.startingMouseX - this.bounds.left}px`;
        this.box.style.top = (this.CurrentY - this.startingMouseY < 0) ?
          `${this.CurrentY - this.bounds.top}px` :
          `${this.startingMouseY - this.bounds.top}px`;
      }
    }
  }

  private startMouseMove(event: MouseEvent): void {
    this.bounds = document.getElementById('image').getBoundingClientRect();
    this.mousePressed = true;
    this.startingMouseX = event.clientX;
    this.startingMouseY = event.clientY;
    if (this.box !== null) {
      this.box = null;
    } else {
      this.box = document.createElement('div');
      this.box.style.top = `${this.startingMouseY - this.bounds.top}px`;
      this.box.style.left = `${this.startingMouseX - this.bounds.left}px`;
      this.box.style.position = 'absolute';
      document.getElementById('image').appendChild(this.box);
    }
  }

  private endMouseMove(event: MouseEvent): void {
    this.box = null;
    this.endMouseX = event.clientX;
    this.endMouseY = event.clientY;
    this.imageService.currentCoordinates.emit({
      endX: this.endMouseX,
      endY: this.endMouseY,
      startY: this.startingMouseY,
      startX: this.startingMouseX
    });
    this.mousePressed = false;
    this.selectionService.selectionSubject.next({
      number: this.selectionArray.length + 1,
      class: this.currentSubject,
      startX: this.startingMouseX,
      startY: this.startingMouseY,
      endX: this.endMouseX,
      endY: this.endMouseY
    });
    // this.getCroppedPicture();
  }

  getCroppedPicture() {
    const selectionBoxBounds = document.getElementsByClassName('box')[0].getBoundingClientRect();
    const cropped = document.getElementsByClassName('crop')[0] as HTMLDivElement;
    const originalImage = document.getElementById('image').getBoundingClientRect();
    cropped.style.width = '500px';
    cropped.style.height = '500px';
    cropped.style.backgroundImage = `url("https://i.imgur.com/9RVulHk.jpg")`;
    cropped.style.backgroundPosition =
      `-${selectionBoxBounds.left - originalImage.left}px -${selectionBoxBounds.top - originalImage.top}px`;
  }

  onMouseDown(event: MouseEvent) {
    this.startMouseMove(event);
  }

  onMouseMove(event: MouseEvent) {
    this.trackMouseMove(event);
  }

  onMouseUp(event: MouseEvent) {
    this.endMouseMove(event);
  }
}
